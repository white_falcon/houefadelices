<?php

namespace App\Controller\Admin;

use App\Entity\Categorie;
use App\Entity\Format;
use App\Entity\Modepaiement;
use App\Entity\Produits;
use App\Entity\Promotion;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
       // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        return $this->render(view: 'admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration pour HOUEFFA DELICES');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateur', 'fa-solid fa-clipboard-user', User::class);
        yield MenuItem::linkToCrud('Categorie', 'fas fa-tags', Categorie::class);
        yield MenuItem::linkToCrud('Format', 'fas fa-list', Format::class);
        yield MenuItem::linkToCrud('Produits', 'fa-brands fa-product-hunt', Produits::class);
        yield MenuItem::linkToCrud('Mode Paiement', 'fa-solid fa-money-check', Modepaiement::class);
        yield MenuItem::linkToCrud('Promotion', 'fa-solid fa-percent', Promotion::class);
    }

}
