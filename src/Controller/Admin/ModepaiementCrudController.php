<?php

namespace App\Controller\Admin;

use App\Entity\Modepaiement;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ModepaiementCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Modepaiement::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Modes Paiements')
            ->setEntityLabelInSingular('Mode Paiement');
    }


    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
