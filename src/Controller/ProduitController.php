<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_produit')]
    public function index(ProduitsRepository $produitsRepository): Response
    {
        $produits = $produitsRepository->findAll();
        return $this->render('produit/produit.html.twig', [
            'produits' => $produits,
        ]);
    }
    #[Route('/produit/{id}', name: 'app_single_produits')]
    public function single(ProduitsRepository $produitsRepository, Produits $produit): Response
    {
        return $this->render('produit/single.produit.html.twig', [
            'singleProduit' => $produit,
        ]);
    }
}
