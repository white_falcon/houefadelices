<?php

namespace App\Controller;

use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ProduitsRepository $produitsRepo): Response
    {
        $gateaux=$produitsRepo->findByCategorie(1,2);
        $petitfours=$produitsRepo->findByCategorie(3,2);
        $viennoiseries=$produitsRepo->findByCategorie(4,2);
        $produits=array_merge($gateaux,$petitfours,$viennoiseries);
        return $this->render('home/index.html.twig', [
           'produits'=>$produits

        ]);
    }

    #[Route('/propos', name: 'app_about')]
    public function prop(): Response
    {
        return $this->render('home/about.html.twig', [

        ]);
    }

    #[Route('/contact', name: 'app_contact')]
    public function contact(): Response
    {
        return $this->render('home/contact.html.twig', [

        ]);
    }



}
