<?php

namespace App\Controller;

use App\Repository\ProduitsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    #[Route('/panier', name: 'app_panier')]
    public function index(SessionInterface $session, ProduitsRepository $produitsRepository): Response
    {
        $panier = $session->get('panier', []);
        $panierWith = [];
        $totals = 0;
        foreach ($panier as $id => $quantite) {
            $panierWith[] = [
                'produits'=>$produitsRepository->find($id),
                'quantite' =>$quantite
            ];

            foreach ($panierWith as $item) {
                $totalItem = $item['produits']->getPrix() * $item['quantite'];
                $totals += $totalItem;
            }
        }
        return $this->render('panier/panier.html.twig', [
            'items' => $panierWith,
            'total' => $totals
        ]);
    }

    #[Route("panier/add/{id}", name: 'app_panier_add')]
    public function add($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);
        if (!empty($panier[$id])){
            $panier[$id]++;
        }else{
            $panier[$id] = 1;
         //$this->addFlash('info-produit','produit ajouté !');
        }
        $session->set('panier', $panier);
        //dd($session->get('panier'));
        return $this->redirectToRoute('app_panier');
    }

    #[Route("panier/remove/{id}", name: 'panier_remove')]
    public function remove($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);
        if (!empty($panier[$id])) {
            unset($panier[$id]);
        }
        $session->set("panier", $panier);
        return $this->redirectToRoute("app_panier");

    }


    #[Route('/panier/verifier', name: 'app_panier_verifier')]
    public function check(SessionInterface $session,ProduitsRepository $produitsRepository): Response
    {
        $panier = $session->get('panier', []);
        $totals=0;
        foreach ($panier as $id => $quantite) {
            $panierWith[] = [
                'produits'=>$produitsRepository->find($id),
                'quantite' =>$quantite
            ];

            foreach ($panierWith as $item) {
                $totalItem = $item['produits']->getPrix() * $item['quantite'];
                $totals += $totalItem;
            }
        }

        return $this->render('panier/checkout.html.twig', [
            'items' => $panierWith,
            'total' => $totals,
        ]);
    }

}
